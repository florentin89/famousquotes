import 'package:flutter/material.dart';
import 'quote.dart';
import 'quote_card.dart';

class FamousQuoteList extends StatefulWidget {
  @override
  _FamousQuoteListState createState() => _FamousQuoteListState();
}

class _FamousQuoteListState extends State<FamousQuoteList> {
  List<Quote> quotes = [
    Quote(
        author: 'Dr. Suess',
        text:
            'You know you’re in love when you can’t fall asleep because reality is finally better than your dreams.'),
    Quote(author: 'Stephen King', text: 'Get busy living or get busy dying.'),
    Quote(
        author: 'Eleanor Roosevelt',
        text:
            'Great minds discuss ideas; average minds discuss events; small minds discuss people.'),
    Quote(
        author: 'David Brinkley',
        text:
            'A successful man is one who can lay a firm foundation with the bricks others have thrown at him.'),
  ];

  @override
  Widget build(BuildContext context) {
    int quoteNo = 0;

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Famous Quotes'),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      body: Column(
          children: quotes
              .map((quote) => QuoteCard(
                    quote: quote,
                    delete: () {
                      setState(() {
                        quotes.remove(quote);
                      });
                    },
                  ))
              .toList()),
    );
  }
}
