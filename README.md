# Famous Quotes

**Version 1.0.0**

**Flutter 1.20**

This project was created in Flutter using Dart and represent a list of famous quotes and also the author of each quote. The user can delete any of that quotes and the screen will be updated. I developed this small app to improve my skills on Google Flutter and to play around with some widgets.

##                                                            VIEW SCREENSHOTS !

![Screenshot 1](demo/FamousQuotes_Photo_1.png)
![Screenshot 2](demo/FamousQuotes_Photo_2.png)

## Contributors
@ Florentin Lupascu <lupascu_florentin@yahoo.com>

## License & Copyright
© Florentin Lupascu, FamousQuotes

Licensed under the [MIT License](LICENSE).
